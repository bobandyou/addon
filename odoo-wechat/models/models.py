# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import AccessDenied, AccessError, UserError, ValidationError

class Wechat(models.Model):
    _inherit = 'res.users'

    wx_openid = fields.Char("微信openid")
    wx_unionid = fields.Char("微信unionid")
    wx_nickname = fields.Char("微信昵称")
    wx_sex = fields.Selection([('0', '未知'), ('1', u'男'), ('2', u'女')], string=u'微信性别')
    login_password = fields.Char("平台绑定登录密码")


class WechatValidation(models.Model):
    _inherit = 'auth.oauth.provider'

    wx_is = fields.Boolean("是否微信扫码登录")
    wx_appid = fields.Char("微信开放平台APPID")
    wx_appsecret = fields.Char("微信开放平台APPSECRET")
    wx_redirect_uri = fields.Char("验证通过后的回调URL")
    wx_code_uri = fields.Char("开放平台请求codeUrl", default='https://open.weixin.qq.com/connect/qrconnect')
    wx_scope = fields.Char("开放平台Scope参数", default='snsapi_login')
    wx_access_token_url = fields.Char("开放平台获取tokenUrl", default='https://api.weixin.qq.com/sns/oauth2/access_token')
    wx_user_info_url = fields.Char("开放平台获取userUrl", default='https://api.weixin.qq.com/sns/userinfo')
    image_128 = fields.Image("公众号二维码", max_width=128, max_height=128)
