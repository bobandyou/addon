# -*- coding: utf-8 -*-
{
    'name': "on_change_function",

    'summary': """
        自动从两个字段中计算销售架构.""",

    'description': """
        本模块将自动计算销售价格其他两个字段（CostPrice和ShippingCosts）的总和。创建新产品或编辑新产品时，可以在“销售”>“产品”下看到此操作。
    """,

    'author': "openerp.hk",
    'website': "https://cdn.openerp.hk",

    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}
