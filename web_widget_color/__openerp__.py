# -*- encoding: utf-8 -*-
############################################################################
#
# Odoo, Open Source Web Widget Color
#
##############################################################################
{
    'name': "Web Widget Color",
    'category': "web",
    'version': "1.0",
    "author": "openerp.hk",
    'depends': ['base', 'web'],
    'data': [
        'view/web_widget_color_view.xml'
    ],
    'qweb': [
        'static/src/xml/widget.xml',
    ],
    'auto_install': False,
    'installable': True,
    'web_preload': True,
}
