# -*- coding: utf-8 -*-
{
    'name': "xpath_expressions",

    'summary': """
        xpath examples""",

    'description': """
        本模块展示了sale>products下的一些xpath示例。它具有创建新选项卡、新组和新字段的选项.
    """,

    'author': "openerp.hk",
    'website': "https://cdn.openerp.hk",

    # Categories can be used to filter modules in modules listing

    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}
