# -*- coding: utf-8 -*-

from openerp import models, fields, api

class on_change_function(models.Model):
    #继承模型product.template
    _inherit = 'product.template'
    #创建两个新的字段 (CostPrice and ShippingCost) 于模型 product.template
    CostPrice = fields.Float('Buy price')
    ShippingCost = fields.Float('Shipping Cost')
    FieldAfterGroup = fields.Char(string='Field After Group')
    FieldNewPage = fields.Char(string='Field New Page')

    #当字段CostPrice或字段ShippingCost更改时，将调用此方法。
    def on_change_price(self,cr,user,ids,CostPrice,ShippingCost,context=None):
	#计算总价
	total = CostPrice + ShippingCost
        res = {
            'value': {
		#确定总价格standard_price.
                'standard_price': total
	      }
	}
	#返回值到视图.
	return res
